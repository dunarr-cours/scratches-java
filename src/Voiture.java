public class Voiture extends Vehicule {
    public static int nbRoues = 4;
    public int nbPortes;

    public Voiture(String couleur, String immatriculation, Conducteur conducteur) {
        super(couleur, immatriculation,  conducteur);
    }

    public Voiture(String couleur, String immatriculation) {
        super(couleur, immatriculation, null);
    }

    public Voiture(String couleur, String immatriculation, int kilometrage) {
        super(couleur, immatriculation, null);
        this.kilometrage = kilometrage;
    }

    public static int getNbRoues() {
        return nbRoues;
    }


    public String toString(){
        return "Une voiture " + couleur + " immatriculée: " +  immatriculation;
    }
}
