import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scann = new Scanner(System.in);
        /*boolean monBool = true;


        byte maVar = 1;  // stoqué sur un octet
        short monShort = 2; // stoqué sur 16 bits
        char monCaractere = '1';
        int monNombre = 1; // stoqué sur 32 bits
        long monAutreNombre = 1; // stoqué sur 64 bits
        float monNombreAVirgule = 1.4f;
        double uneAutreVariable = 1.5;

        // /!\ String est une classe, pas un type primitif
        String maChaine = "Hello World";

        int[] monTableau = {1, 0, 5};
        int[][] monAutreTableau = {
                {0, 1, 4},
                {2, 3},
                {4, 5, 8}
        };
        int[] monTableau2 = new int[10];
        String[] monTableau3 = new String[10];
        int[][] monTableau4 = new int[10][];
        int[][] monTableau5 = new int[10][10];

        int variable1 = 1;
        byte variable2 = (byte) variable1;

        byte variable3 = 1;
        int variable4 = variable3;

        int variable5;
        variable5 = 3;
        variable5 = 2;
        variable5 = (int)5.9;

        variable5 += 1; // variable5 = variable5 + 1
        variable5-=1;
        variable5/=2;
        variable5*=2;
        variable5--;
        variable5++;

        int variable6 = 5 + 3;
        variable6 = 5 - 3;
        variable6 = 5 * 3;
        variable6 = 5 / 2;  // variable6 est un int, le resultat est donc tronqué, ici 5/2 = 2
        variable6 = 5 % 2; // 1

        boolean variable7;
        variable7 = 5 > variable6;
        variable7 = 5 < 3;
        variable7 = 5 <= 3;
        variable7 = 5 >= 3;
        variable7 = 5 == '5';
        variable7 = 5 != '5';

        variable7 = true && false;
        variable7 = true || false;
        variable7 = !false;

        if (5 > 3){
            // j'écrit du super code
        } else if (3 == 3){
            // la première condition est fausse mais la seconde est vraie
        } else {
            // les deux autres conditions sont fausses
        }

        switch (variable6){
            case 1:
                //ecrire du code
                break;
            case 2:
                //ecrire du code
                break;
            case 3:
                //ecrire du code
                break;
        }

        int i = 0;
        while (i < 10){
            i++;
        }

        i = 10;
        do { // le bloc de code est executé au moins une première fois
            i++;
        } while (i < 10);

        for (int j = 0; j < 10; j++) {
            // Cette instruction sera répétée 10 X
        }

        int[] tableau = {0, 5, 9, 10};

        for(int number: tableau){
            // je peux faire ce quer je veux avec number
        }

        int[][] tableau2 = {
                {0, 1, 4},
                {2, 3},
                {4, 5, 8}
        };

        for(int[] line: tableau2){
            for (int number: line) {
                // je peux faire ce quer je veux avec number
            }
        }

        System.out.println(args);

        System.out.println("Tapez un nombre:");
        System.out.print("> ");
        int input = scann.nextInt();
        System.out.println("Votre nombre: " + input);

        int number = (int)(Math.random()*101);*/
        System.out.println("Helo");
        int b = scann.nextInt();
        int c = scann.nextInt();
        String a = scann.nextLine();
        System.out.println("========");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println("========");
        a = scann.nextLine();
        System.out.println(a);
    }
}

