public abstract class Vehicule {
    public static int nbRoues;

    protected String immatriculation;
    protected int kilometrage;
    protected String couleur;
    protected Conducteur conducteur;

    public void rouler(int longTrajet) {
        kilometrage = kilometrage + longTrajet;
    }

    public boolean rouler() {
        kilometrage = kilometrage + 500;
        return true;
    }

    public Conducteur getConducteur() {
        return conducteur;
    }

    public Vehicule setConducteur(Conducteur conducteur) {
        this.conducteur = conducteur;
        return this;
    }

    public Vehicule(String couleur, String immatriculation, Conducteur conducteur) {
        kilometrage = 0;
        this.immatriculation = immatriculation;
        this.couleur = couleur;
        this.conducteur = conducteur;
    }

}
